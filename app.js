// Problem functions
import Cuadratic from './problems/cuadratic';
import PenalizedCuadratic from './problems/penalized-cuadratic';

// Gradient functions
import newtonGradient from './gradients/newton';
import newtonDescGradient from './gradients/newton-desc';
import newtonMomemtumGradient from './gradients/newton-momemtum';
import newtonClimbingGradient from './gradients/newton-climbing';

// Random functions
import randomGauss from './random/gauss';
import randomPowerLaw from './random/power-law';

// Methods for solving
import newtonRaphson from './methods/newton-raphson';

// Declaring variables
let problems = [
  new Cuadratic(),
  new PenalizedCuadratic()
];

let gradients = [
  newtonGradient(),
  newtonDescGradient({h: 0.4}),
  newtonMomemtumGradient({alpha: 0.2, miu: 0.8, delta: [0,0]}),
  newtonClimbingGradient({sigma: 0.2, rand: randomGauss()}),
  newtonClimbingGradient({sigma: 0.2, rand: randomPowerLaw({alpha: 2})})
];

let xs = [2,3];
let x1, x2;

// Executing
// [x1, x2] = newtonRaphson(problems[0], gradients[0], xs); // Problem + Newton: exp-30: 98 iterations
// [x1, x2] = newtonRaphson(problems[0], gradients[1], xs); // Problem + Newton Desc: exp-30: 43 iterations
// [x1, x2] = newtonRaphson(problems[0], gradients[2], xs); // Problem + Newton Momemtum: exp-30: 46 iterations
// [x1, x2] = newtonRaphson(problems[0], gradients[3], xs, 1000000); // Problem + Newton Climbing + Gauss Distribution: exp-30: does not work with 1.000.000 iterations :(
// [x1, x2] = newtonRaphson(problems[0], gradients[4], xs, 1000000); // Problem + Newton Climbing + Power Law Distribution: exp-30: does not work with 1.000.000 iterations :(
[x1, x2] = newtonRaphson(problems[1], gradients[0], xs); // Problem + Newton: Factible solution in 8 iterations
// [x1, x2] = newtonRaphson(problems[1], gradients[1], xs, 1000000); // Problem + Newton Desc: No factible solutions over 1.000.000 iterations
// [x1, x2] = newtonRaphson(problems[1], gradients[2], xs, 1000000); // Problem + Newton Momemtum: exp-30: No factible solutions over 1.000.000 iterations
// [x1, x2] = newtonRaphson(problems[1], gradients[3], xs); // Problem + Newton Climbing + Gauss Distribution: Factible solution in 9 iterations
// [x1, x2] = newtonRaphson(problems[0], gradients[4], xs, 1000000); // Problem + Newton Climbing + Power Law Distribution: exp-30: No factible solutions over 1.000.000 iterations
console.log(`Best x1: ${x1}`);
console.log(`Best x2: ${x2}`);
console.log(randomGauss())



