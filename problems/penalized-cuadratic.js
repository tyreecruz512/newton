import Cuadratic from './cuadratic';


export default class PenalizedCuadratic extends Cuadratic {
  penalizedValues = [5, 8, 500];
  
  penalizedFunctions = [
    ([x1, x2]) => this.penalizedValues[0] * (x1 < 1 ? 1 - x1 : 0),
    ([x1, x2]) => this.penalizedValues[1] * (x2 < 2 ? 2 - x2 : 0),
    ([x1, x2]) => this.penalizedValues[2] * (x1 + 2 * x2 > 6 ? x1 + 2 * x2 - 6 : 0)
  ];

  penalizedGradients = [
    [   // x1
      ([x1, x2]) => this.penalizedValues[0] * (x1 < 1 ? -1 : 0),
      ([x1, x2]) => this.penalizedValues[2] * (x1 + 2 * x2 > 6 ? 1 : 0)
    ],
    [   // x2
      ([x1, x2]) => this.penalizedValues[1] * (x2 < 2 ? -1 : 0),
      ([x1, x2]) => this.penalizedValues[2] * (x1 + 2 * x2 > 6 ? 2 : 0)
    ]
  ];

  f(xs) {
    return super.f(xs) + this.penalizedFunctions.reduce((total, fn) => total + fn(xs), 0);
  }

  gradient(xs) {
    let gradient = super.gradient(xs);
    return [
      gradient[0] + this.penalizedGradients[0].reduce((total, fn) => total + fn(xs), 0),
      gradient[1] + this.penalizedGradients[1].reduce((total, fn) => total + fn(xs), 0)
    ];
  }

  factible([x1, x2]) {
    return x1 >= 1 && x2 >= 2 && x1 + 2 * x2 <= 6;
  }
}
