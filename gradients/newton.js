 export default function () {
  return (problem, xs) => {
    let gradient = problem.gradient(xs);
    let hesian = problem.detHesian(xs);

    return xs.map((x, i) => x + (-gradient[i]) / hesian);
  }
}