export default function(options){

  return (problem, xs) => {
    let gradient = problem.gradient(xs);
    return xs.map((x, i) => x + (-gradient[i]) * options.h);
  }
}