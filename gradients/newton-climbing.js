export default function(options){

  return (problem, xs) => {
    let ys = xs.map(x => x + options.sigma * options.rand());
    return problem.f(xs) < problem.f(ys) ? xs : ys;
  }
}